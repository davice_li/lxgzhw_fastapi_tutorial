from fastapi import FastAPI, Response, Cookie
from typing import Optional

app = FastAPI()


@app.post("/set_cookie", summary="设置cookie")
def create_cookie(response: Response):
    response.set_cookie(key="username", value="lxgzhw001")
    return {"message": "cookie设置成功"}


@app.get("/get_cookie", summary="获取cookie")
def get_cookie(username: Optional[str] = Cookie(None)):
    return {"message": f"cookie获取成功：{username}"}
