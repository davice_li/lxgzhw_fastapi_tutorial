from typing import Optional

from fastapi import FastAPI
from fastapi.responses import FileResponse
from pydantic import BaseModel


class Item(BaseModel):
    id: str
    value: str


# 通用响应格式
responses = {
    404: {"description": "Item not found"},
    302: {"description": "The item was moved"},
    403: {"description": "Not enough privileges"},
}

app = FastAPI()


@app.get(
    "/items/{item_id}",
    response_model=Item,
    # 通用响应格式+自定义响应格式
    responses={**responses, 200: {"content": {"image/png": {}}}},

)
async def read_item(item_id: str, img: Optional[bool] = None):
    if img:
        return FileResponse("1.jpg", media_type="image/jpg")
    else:
        return {"id": "foo", "value": "there goes my hero"}
