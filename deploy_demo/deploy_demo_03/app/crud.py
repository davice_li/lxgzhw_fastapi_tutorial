from sqlalchemy.orm import Session
from . import models, schemas


def get_user(db: Session, user_id: int):
    """
    获取用户
    :param db: 数据库链接对象
    :param user_id: 用户id
    :return: 用户orm对象
    """
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    """
    根据email获取用户
    :param db: 数据库链接对象
    :param email: 邮箱
    :return: 用户orm对象
    """
    return db.query(models.User).filter(models.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    """
    分页获取用户
    :param db:数据库链接对象
    :param skip: 跳过多少
    :param limit: 一次选多少
    :return: 用户orm列表
    """
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    """
    创建用户
    :param db:数据库链接对象
    :param user: 用户创建模型
    :return: 创建的用户
    """
    # 这里需要改成真实的加密字符串
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = models.User(email=user.email, hashed_password=fake_hashed_password)
    # 添加
    db.add(db_user)
    # 提交事务
    db.commit()
    # 刷新
    db.refresh(db_user)
    return db_user


def get_items(db: Session, skip: int = 0, limit: int = 100):
    """
    获取items
    :param db: 数据库链接对象
    :param skip: 跳过多少个
    :param limit: 每次限制多少个
    :return: item列表
    """
    return db.query(models.Item).offset(skip).limit(limit).all()


def create_user_item(db: Session, item: schemas.ItemCreate, user_id: int):
    """
    创建item
    :param db: 数据库链接对象
    :param item: 添加的item
    :param user_id: 用户id
    :return: 创建的item
    """
    db_item = models.Item(**item.dict(), owner_id=user_id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item
