from fastapi import Depends, FastAPI
from .dependencies import get_query_token, get_token_header
from .internal import admin
from .routers import items, users

# 添加全局依赖
app = FastAPI(dependencies=[Depends(get_query_token)])

app.include_router(users.router)
app.include_router(items.router)
# 在添加路由时配置参数
app.include_router(
    admin.router,
    prefix="/admin",
    tags=["admin"],
    # 在添加路由时设置依赖
    dependencies=[Depends(get_token_header)],
    responses={418: {"description": "I'm a teapot"}},
)


# app自己也可以管理一些路由
@app.get("/")
async def root():
    return {"message": "Hello Bigger Applications!"}
