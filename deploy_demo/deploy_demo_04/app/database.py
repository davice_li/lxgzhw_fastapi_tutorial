from contextvars import ContextVar
import peewee

DATABASE_NAME = "user.db"
db_state_default = {"closed": None, "conn": None, "ctx": None, "transactions": None}
db_state = ContextVar("db_state", default=db_state_default.copy())


class PeeweeConnectionState(peewee._ConnectionState):
    def __init__(self, **kwargs):
        super().__setattr__("_state", db_state)
        super().__init__(**kwargs)

    def __setattr__(self, name, value):
        self._state.get()[name] = value

    def __getattr__(self, name):
        return self._state.get()[name]


# 创建数据库连接对象
db = peewee.SqliteDatabase(DATABASE_NAME, check_same_thread=False)
# 数据库连接状态
db._state = PeeweeConnectionState()
