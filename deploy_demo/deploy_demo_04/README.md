## 运行
```shell
uvicorn app.main:app --reload --host 0.0.0.0 --port 9300
```

## 访问
http://192.168.204.141:9300/docs

## 生成镜像
```shell
docker build -t deploy_demo_04 .
```
## 停止之前的容器
```shell
docker stop deploy_demo_03
```

## 运行容器
```shell
docker run -d --name deploy_demo_04 -p 80:80 deploy_demo_04
```

## 查看容器
```shell
docker ps -a
```

## 访问
http://192.168.204.141/docs