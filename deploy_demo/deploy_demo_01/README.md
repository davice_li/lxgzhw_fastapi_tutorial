## 生成镜像
```shell
docker build -t deploy_demo_01 .
```

## 运行容器
```shell
docker run -d --name deploy_demo_01 -p 80:80 deploy_demo_01
```

## 访问
http://192.168.204.141/docs