## 运行
```shell
uvicorn app.main:app --reload --host 0.0.0.0 --port 9300
```

## 访问
http://192.168.204.141:9300/docs

## 删除之前的镜像
```shell
docker rmi demo
```

## 生成镜像
```shell
docker build -t demo .
```
## 停止之前的容器
```shell
docker stop demo
```

## 删除之前的容器
```shell
docker rm demo
```

## 运行容器
```shell
docker run -d --name demo -p 80:80 demo
```

## 查看容器
```shell
docker ps -a
```

## 访问
http://192.168.204.141/docs