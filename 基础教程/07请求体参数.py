from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

# 准备数据
users = [
    {'id': 1, 'name': '张三', 'age': 33},
    {'id': 2, 'name': '张三', 'age': 33},
    {'id': 3, 'name': '张三', 'age': 33},
    {'id': 4, 'name': '张三', 'age': 33},
    {'id': 5, 'name': '张三', 'age': 33},
]


# 定义请求体 json传参
class UserItem(BaseModel):
    id: int
    name: str
    age: int


# 请求体参数
@app.post("/api/user")
async def add_user(user: UserItem):
    # item能够直接转换为dict类型
    users.append(dict(user))
    print(users)
    return user
