from fastapi import Depends, FastAPI
from fastapi.security import OAuth2PasswordBearer

app = FastAPI()

# 创建auth
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


# token依赖auth
# 一旦添加了auth依赖，必须要校验权限以后，才能够使用接口
@app.get("/items/")
async def read_items(token: str = Depends(oauth2_scheme)):
    return {"token": token}
