from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


# {item_id}是路径参数
# 路径参数需要在方法中声明，必须同名
@app.get("/api/user/{item_id}")
async def read_item(item_id):
    return {"item_id": item_id}