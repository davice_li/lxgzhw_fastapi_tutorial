from typing import Optional
from fastapi import Depends, FastAPI

app = FastAPI()


# 一个通用的参数和响应
async def common_parameters(q: Optional[str] = None, skip: int = 0, limit: int = 100):
    return {"q": q, "skip": skip, "limit": limit}


# 使用依赖注入
@app.get("/items/")
async def read_items(commons: dict = Depends(common_parameters)):
    return commons


# 使用依赖注入
@app.get("/users/")
async def read_users(commons: dict = Depends(common_parameters)):
    return commons
