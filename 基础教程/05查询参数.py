from fastapi import FastAPI

app = FastAPI()

# 准备数据
users = [
    {'id': 1, 'name': '张三', 'age': 33},
    {'id': 2, 'name': '张三', 'age': 33},
    {'id': 3, 'name': '张三', 'age': 33},
    {'id': 4, 'name': '张三', 'age': 33},
    {'id': 5, 'name': '张三', 'age': 33},
]


# 查询参数
@app.get("/api/user")
async def get_user(index: int):
    return {"file_path": users[index]}
