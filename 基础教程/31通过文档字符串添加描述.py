from typing import Optional, Set
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None
    tags: Set[str] = []


@app.post(
    "/items/",
    response_model=Item,
    # 概要
    summary="创建一个item",
    # 设置api为已弃用
    deprecated=True
)
async def create_item(item: Item):
    """
    通过文档字符串添加描述

    - **name**: 参数1的描述
    - **description**: 参数2的描述。。。
    - **price**: 参数3的描述。。。
    - **tax**: 参数4的描述。。。
    - **tags**: 参数5的描述。。。
    """
    return item
