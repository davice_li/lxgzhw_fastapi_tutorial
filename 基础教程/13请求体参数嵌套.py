from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional

app = FastAPI()


class ChildItem(BaseModel):
    name: str


class FatherItem(BaseModel):
    name: str
    # 请求体参数嵌套
    child: Optional[ChildItem]


@app.post("/api/user")
async def add_user(item: FatherItem):
    return {"item": dict(item)}
