from fastapi import FastAPI, Path

app = FastAPI()


# 路径参数校验
@app.get("/api/user/{id}")
async def add_user(id: int = Path(default=None, alias='id', title='用户id是必须的')):
    return {"路径参数": id}
