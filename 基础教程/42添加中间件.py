import time
from fastapi import FastAPI
from starlette.requests import Request

app = FastAPI()


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    print("消耗时间： ", process_time)
    response.headers["X-Process-Time"] = str(process_time)
    return response


@app.get("/")
async def main():
    time.sleep(3)
    return {"message": "Hello World"}
