from fastapi import FastAPI

app = FastAPI()


# 路径转换器： 声明file_path必须是路径类型
# 在实际测试中，任意类型数据都能够通过
@app.get("/api/user/{file_path:path}")
async def read_file(file_path: str):
    return {"file_path": file_path}
