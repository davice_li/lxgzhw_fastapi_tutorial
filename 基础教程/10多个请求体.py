from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Item1(BaseModel):
    name: str


class Item2(BaseModel):
    name: str


# 多个请求体
@app.post("/api/user")
async def add_user(item1: Item1, item2: Item2):
    return {"item1": dict(item1), "item2": dict(item2)}
