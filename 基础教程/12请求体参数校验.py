from fastapi import FastAPI
from pydantic import BaseModel, Field
from typing import Optional

app = FastAPI()


class Item(BaseModel):
    name: str
    # 请求体参数校验
    desc: Optional[str] = Field(
        None,
        title="请求体参数校验",
        min_length=2,
        max_length=100
    )


@app.post("/api/user")
async def add_user(item: Item):
    return {"item": dict(item)}
