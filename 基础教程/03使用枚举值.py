from fastapi import FastAPI
from enum import Enum


# 预定义值
# 这是一个枚举值，规定请求数据必须是其中的一个
class ModelName(str, Enum):
    a = "aaa"
    b = "bbb"
    c = "ccc"


app = FastAPI()


# 使用枚举值，必须是枚举中的一个，否则就会报错
@app.get("/api/user/{model_name}")
async def get_model(model_name: ModelName):
    if model_name == ModelName.a:  # 通过键判断
        return {"model_name": model_name, "message": "获取到了什么。。。"}

    if model_name.value == "bbb":  # 通过值判断
        return {"model_name": model_name, "message": "请求了bbbbbb"}

    return {"model_name": model_name, "message": "cccccccccccc"}
