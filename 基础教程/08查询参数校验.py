from fastapi import FastAPI, Query
from typing import Optional

app = FastAPI()


# 查询参数校验
# regex='^a‘ 可以通过正则表达式实现灵活的校验
@app.get("/api/user")
async def add_user(query: Optional[str] = Query(default="默认值", min_length=4, max_length=24, regex='^a')):
    return {"查询参数": query}
