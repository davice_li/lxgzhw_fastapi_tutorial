from fastapi import FastAPI

app = FastAPI()


# 指定响应状态码
@app.post("/items/", status_code=201)
async def create_item(name: str):
    return {"name": name}
