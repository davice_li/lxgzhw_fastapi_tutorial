from fastapi import FastAPI, File, UploadFile

app = FastAPI()


# 创建文件，接收文件流
@app.post("/files/")
async def create_file(file: bytes = File(...)):
    # 将上传的文件保存到服务器
    with open("../test.jpg", "wb", buffering=1024 * 4) as f:
        f.write(file)
    return {"file_size": len(file)}


# 上次文件，封装文件对象
# 得到的是一个封装了异步方法的tempfile对象
@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile = File(...)):
    return {"filename": file.filename}
