from fastapi import FastAPI
from typing import Optional

app = FastAPI()

# 准备数据
users = [
    {'id': 1, 'name': '张三', 'age': 33},
    {'id': 2, 'name': '张三', 'age': 33},
    {'id': 3, 'name': '张三', 'age': 33},
    {'id': 4, 'name': '张三', 'age': 33},
    {'id': 5, 'name': '张三', 'age': 33},
]


# 查询参数
# 设置查询参数的默认值为1
@app.get("/api/user")
async def get_user(index: Optional[int] = 1):
    return {"file_path": users[index]}
