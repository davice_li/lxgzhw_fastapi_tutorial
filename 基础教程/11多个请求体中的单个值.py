from fastapi import FastAPI, Body
from pydantic import BaseModel

app = FastAPI()


class Item1(BaseModel):
    name: str


class Item2(BaseModel):
    name: str


# 多个请求体中的单个值
@app.post("/api/user")
async def add_user(item1: Item1, item2: Item2, desc: str = Body(default="默认值")):
    return {"item1": dict(item1), "item2": dict(item2), "desc": desc}
