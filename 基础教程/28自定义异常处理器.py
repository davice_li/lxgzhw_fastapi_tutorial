from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse


# 异常类
class UnicornException(Exception):
    def __init__(self, name: str):
        self.name = name


app = FastAPI()


# 异常捕获
@app.exception_handler(UnicornException)
async def unicorn_exception_handler(request: Request, exc: UnicornException):
    # 添加自定义的响应
    return JSONResponse(
        status_code=418,
        content={"message": f"Oops! {exc.name} did something. There goes a rainbow..."},
    )


@app.get("/unicorns/{name}")
async def read_unicorn(name: str):
    if name == "yolo":
        raise UnicornException(name=name)
    return {"unicorn_name": name}
