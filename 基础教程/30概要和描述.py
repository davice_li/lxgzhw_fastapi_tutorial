from typing import Optional, Set
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None
    tags: Set[str] = []


@app.post(
    "/items/",
    response_model=Item,
    # 概要
    summary="创建一个item",
    # 描述
    description="这是关于这个api的描述。。。",

)
async def create_item(item: Item):
    return item
