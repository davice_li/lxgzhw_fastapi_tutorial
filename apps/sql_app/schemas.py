from typing import List, Optional
from pydantic import BaseModel


class ItemBase(BaseModel):
    title: str
    description: Optional[str] = None


class ItemCreate(ItemBase):
    pass


class Item(ItemBase):
    id: int
    owner_id: int

    class Config:
        orm_mode = True


# 用户基本模型
class UserBase(BaseModel):
    email: str


# 用户创建模型
class UserCreate(UserBase):
    password: str


# 用户展示模型
class User(UserBase):
    id: int
    is_active: bool
    items: List[Item] = []

    # 使用orm
    class Config:
        orm_mode = True
