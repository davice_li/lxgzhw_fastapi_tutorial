from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = "Awesome API"
    admin_email: str = 'test@qq.com'
    items_per_user: int = 50
